FROM ubuntu:devel

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    libheif-examples \
    texlive-fonts-extra \
    texlive-latex-extra \
    img2pdf \
    imagemagick \
    inkscape \
 && rm -rf /var/lib/apt/lists/*

RUN ln -sfv /usr/share/zoneinfo/Australia/Sydney /etc/localtime

